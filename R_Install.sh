# Grabs your version of Ubuntu as a BASH variable
# CODENAME=`grep CODENAME /etc/lsb-release | cut -c 18-`
# Appends the CRAN repository to your sources.list file
# sudo sh -c 'echo "deb http://cran.rstudio.com/bin/linux/ubuntu $CODENAME" >> /etc/apt/sources.list'
# Adds the CRAN GPG key, which is used to sign the R packages for security.
#sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-key E084DAB9

#sudo apt-get update
#sudo apt-get install r-base r-base-dev

sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E084DAB9
sudo echo "deb http://cran.wustl.edu/bin/linux/ubuntu xenial/" | sudo tee -a /etc/apt/sources.list
sudo apt update
sudo apt upgrade r-base r-base-dev
sudo apt update
sudo apt upgrade

